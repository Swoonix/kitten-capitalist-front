import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';

import { RouterModule, Routes } from '@angular/router';

/** MaterialModules */
import { MatButtonModule, MatCheckboxModule, MatToolbarModule, MatIconModule, MatListModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBadgeModule } from '@angular/material/badge';


import { LayoutModule } from '@angular/cdk/layout';
import { ProductsComponent } from './products/products.component';
import { BigvaluePipe } from './bigvalue.pipe';
import { ManagersComponent } from './managers/managers.component';
import { ManagerComponent } from './manager/manager.component';
import { UnlocksComponent } from './unlocks/unlocks.component';
import { UnlockComponent } from './unlock/unlock.component';
import { CashUpgradesComponent } from './cash-upgrades/cash-upgrades.component';
import { CashUpgradeComponent } from './cash-upgrade/cash-upgrade.component';
import { ProductUnlockComponent } from './product-unlock/product-unlock.component';
import { ProductUnlocksComponent } from './product-unlocks/product-unlocks.component';


const appRoutes: Routes = [
  { path: 'products', component: ProductsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ProductComponent,
    ProductsComponent,
    BigvaluePipe,
    ManagersComponent,
    ManagerComponent,
    UnlocksComponent,
    UnlockComponent,
    CashUpgradesComponent,
    CashUpgradeComponent,
    ProductUnlockComponent,
    ProductUnlocksComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    LayoutModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    FlexLayoutModule,
    MatProgressBarModule,
    MatDialogModule,
    MatSnackBarModule,
    MatBadgeModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    ManagersComponent,
    UnlocksComponent,
    CashUpgradesComponent,
    ProductUnlocksComponent
  ]
})
export class AppModule { }
