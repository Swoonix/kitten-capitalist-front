import { Pallier } from './../world';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  @Input()
  set manager(value: Pallier) {
    this.managerP = value;
  }

  constructor(private service: RestserviceService, private snackBar: MatSnackBar) {
    this.imageUrl = this.service.getServerImage();
  }

  managerP: Pallier;
  imageUrl: string;
  isBuyable: boolean;

  @Input() money: any;

  @Output() notifyMoney: EventEmitter<any> = new EventEmitter();
  @Output() notifyUnlock: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    setInterval(() => { this.checkBuyable(); }, 100);
  }

  openSnackBar() {
    this.snackBar.open('Manager débloqué !', 'Fermer', {duration: 1000});
  }

  hireManager() {
    this.money = this.money - this.managerP.seuil;
    this.notifyMoney.emit(this.money);
    this.managerP.unlocked = true;
    this.notifyUnlock.emit(this.managerP.unlocked);
    this.openSnackBar();
    this.service.putManager(this.managerP);
  }

  checkBuyable() {
    this.isBuyable = (this.money > this.managerP.seuil && !this.managerP.unlocked) ? true : false;
  }

}
