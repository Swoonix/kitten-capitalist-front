import { Component, OnInit, Inject } from '@angular/core';
import { World } from '../world';
import { RestserviceService } from '../restservice.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.css']
})
export class ManagersComponent implements OnInit {
  managers;
  constructor(public dialogRef: MatDialogRef<ManagersComponent>, @Inject(MAT_DIALOG_DATA) public data: World) {
  }

  ngOnInit() {
  }

  public close() {
    this.dialogRef.close();
  }

  onMoneyChange(n: number) {
    this.data.money = n;
  }

  onManagerUnlocked(b: boolean, i: number) {
    this.data.managers.pallier[i].unlocked = b;
    this.data.products.product[i].managerUnlocked = b;
  }

}
