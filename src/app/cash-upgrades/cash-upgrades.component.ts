import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, OnInit, Inject } from '@angular/core';
import { World } from '../world';

@Component({
  selector: 'app-cash-upgrades',
  templateUrl: './cash-upgrades.component.html',
  styleUrls: ['./cash-upgrades.component.css']
})
export class CashUpgradesComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<CashUpgradesComponent>, @Inject(MAT_DIALOG_DATA) public data: World) { }

  ngOnInit() {
  }

  public close() {
    this.dialogRef.close();
  }

}
