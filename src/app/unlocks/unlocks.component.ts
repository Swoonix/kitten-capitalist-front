import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { World } from '../world';

@Component({
  selector: 'app-unlocks',
  templateUrl: './unlocks.component.html',
  styleUrls: ['./unlocks.component.css']
})
export class UnlocksComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UnlocksComponent>, @Inject(MAT_DIALOG_DATA) public data: World) { }

  ngOnInit() {
  }

  public close() {
    this.dialogRef.close();
  }

}
