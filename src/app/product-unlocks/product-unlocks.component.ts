import { Pallier } from './../world';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductUnlockComponent } from '../product-unlock/product-unlock.component';
import { World } from '../world';

@Component({
  selector: 'app-product-unlocks',
  templateUrl: './product-unlocks.component.html',
  styleUrls: ['./product-unlocks.component.css']
})
export class ProductUnlocksComponent implements OnInit {

  unlock: Pallier;

  constructor(public dialogRef: MatDialogRef<ProductUnlockComponent>, @Inject(MAT_DIALOG_DATA) public data: World) {
    this.unlock = data.products.product[0].palliers.pallier[0];
  }

  ngOnInit() {
  }

  public close() {
    this.dialogRef.close();
  }

}
