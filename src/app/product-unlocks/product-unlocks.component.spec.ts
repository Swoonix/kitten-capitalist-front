import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUnlocksComponent } from './product-unlocks.component';

describe('ProductUnlocksComponent', () => {
  let component: ProductUnlocksComponent;
  let fixture: ComponentFixture<ProductUnlocksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUnlocksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUnlocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
