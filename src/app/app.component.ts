import { CashUpgradesComponent } from './cash-upgrades/cash-upgrades.component';
import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { RestserviceService } from './restservice.service';
import { World, Product } from './world';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ManagersComponent } from './managers/managers.component';
import { UnlocksComponent } from './unlocks/unlocks.component';
import { ProductUnlocksComponent } from './product-unlocks/product-unlocks.component';
import { ProductComponent } from './product/product.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  server: string;
  serverImg: string;
  world: World = new World();
  username: string;
  qtmulti: any;

  @ViewChildren(ProductComponent) productsComponent: QueryList<ProductComponent>;

  constructor(private service: RestserviceService, public dialog: MatDialog, private snackBar: MatSnackBar) {
    this.server = service.getServer();
    this.serverImg = service.getServerImage();
    service.getWorld().then(world => {
      this.world = world;
    });
    this.username = localStorage.getItem('username');
    if (this.username === '' || this.username === null) {
      const nb = Math.floor(Math.random() * 10000).toString();
      this.username = 'Kitten' + nb;
      this.onUsernameChanged();
    } else {
      this.service.setUser(this.username);
    }
  }

  ngOnInit(): void {
    this.qtmulti = 1;
  }

  onProductionDone(p: Product) {
    this.world.money = this.world.money + p.quantite * p.revenu;
  }

  onMoneyChange(n: number) {
    this.world.money = n;
  }

  switchValue() {
    switch (this.qtmulti) {
      case 1: {
        this.qtmulti = 10;
        break;
      }
      case 10: {
        this.qtmulti = 100;
        break;
      }
      case 100: {
        this.qtmulti = 'Max';
        break;
      }
      default: {
        this.qtmulti = 1;
        break;
      }
    }
  }

  onUsernameChanged() {
    localStorage.setItem('username', this.username);
    this.service.setUser(this.username);
    this.service.getWorld().then(world => {
      this.world = world;
    });
  }

  openManagerDialog(): void {
    const dialogRef = this.dialog.open(ManagersComponent, {
      data: this.world
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openAllUnlockDialog(): void {
    const dialogRef = this.dialog.open(UnlocksComponent, {
      data: this.world
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openUnlockDialog(): void {
    const dialogRef = this.dialog.open(ProductUnlocksComponent, {
      data: this.world
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openCashUpgradeDialog(): void {
    const dialogRef = this.dialog.open(CashUpgradesComponent, {
      data: this.world
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  isManagerBuyable() {
    const test = this.world.managers.pallier.some(element => {
      if (element.unlocked === false && this.world.money > element.seuil) {
        return false;
      }
    });
    return test;
  }

  onBuyProduct(p: Product) {
    this.calcPossibleUnlock();
  }

  calcPossibleUnlock() {
    let possible = true;
    // Pour chaque allunlock
    for (const allunlock of this.world.allunlocks.pallier) {
      // S'il n'est pas déjà débloqué, on test
      if (!allunlock.unlocked) {
        // On vérifie pour tous les produits
        for (const product of this.world.products.product) {
          // Si l'on ne peut pas l'acheter pour un des produits, on ne teste plus
          if (possible) {
            possible = (product.quantite >= allunlock.seuil) ? true : false;
          }
        }
        // Si le boolean est toujours true, on peut unlock
        if (possible) {
          allunlock.unlocked = true;
          this.openSnackBar();
          // on va faire les modifs dans chaque produit
          this.productsComponent.forEach(p => p.calcUpgrade(allunlock));
        }
      }
    }
  }

  openSnackBar() {
    this.snackBar.open('Bonus général débloqué !', 'Fermer', { duration: 1000 });
  }
}
