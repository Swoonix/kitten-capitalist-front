import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { World, Product } from '../world';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  world: World = new World();
  products;
  qtmulti: any;

  constructor(private service: RestserviceService) {
    service.getWorld().then(world => {
      this.world = world;
    });
  }

  ngOnInit() {
    this.qtmulti = 1;
  }

  onProductionDone(p: Product) {
    this.world.money = this.world.money + p.revenu * p.quantite;
  }

  onMoneyChange(n: number) {
    this.world.money = n;
  }

  switchValue() {
    switch (this.qtmulti) {
      case 1: {
        this.qtmulti = 10;
        break;
      }
      case 10: {
        this.qtmulti = 100;
        break;
      }
      case 100: {
        this.qtmulti = 'Max';
        break;
      }
      default: {
        this.qtmulti = 1;
        break;
      }
    }
  }
}
