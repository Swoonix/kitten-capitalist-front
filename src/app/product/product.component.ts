import { Pallier } from './../world';
import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { Product } from '../world';
import { RestserviceService } from '../restservice.service';
import { MatSnackBar } from '@angular/material';

declare var require;
const ProgressBar = require('progressbar.js');

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {
  progressbar: any;
  imageUrl: string;
  product: Product;
  lastUpdate: any;
  // tslint:disable-next-line:variable-name
  _qtmulti: string;
  nbProduit: number;
  totalPrice: any;
  isBuyable: boolean;
  quantityGain: number;
  isRunning: boolean;

  @ViewChild('bar') progressBarItem;

  @Input()
  set prod(value: Product) {
    this.product = value;

    if (this.product.quantite > 0) {
      if (this.product.managerUnlocked === true) {
        this.startFabrication();
      }

      if (this.product && this.product.timeleft > 0) {
        this.lastUpdate = Date.now();
        const progress = (this.product.vitesse - this.product.timeleft) / this.product.vitesse;
        this.progressbar.set(progress);
        this.progressbar.animate(1, { duration: this.product.timeleft });
      }
    }

  }
  
  @Input()
  set qtmulti(value: string) {
    this._qtmulti = value;
    if (this._qtmulti && this.product) { this.calcMaxCanBuy(); }
  }

  @Input() money: any;

  @Output() notifyProduction: EventEmitter<Product> = new EventEmitter<Product>();
  @Output() notifyMoney: EventEmitter<any> = new EventEmitter();
  @Output() notifyBuy: EventEmitter<any> = new EventEmitter();

  constructor(private service: RestserviceService, private snackBar: MatSnackBar) {
    this.imageUrl = this.service.getServerImage();
  }

  ngOnInit() {
    this.isRunning = false;
    this.progressbar = new
      ProgressBar.Line(this.progressBarItem.nativeElement, {
        strokeWidth: 5, color:
          '#3f51b5'
      });
    setInterval(() => { this.calcScore(); this.calcMaxCanBuy(); this.checkIsRunning(); }, 100);
  }

  checkIsRunning() {
    if (this.product.managerUnlocked && !this.isRunning) {
      this.isRunning = true;
      this.startFabrication();
    }
  }

  startFabrication() {
    // On vérifie d'abord que la quantité de produit ne soit pas nulle
    if (this.product.quantite > 0) {
      this.product.timeleft = this.product.vitesse;
      this.lastUpdate = Date.now();
      this.progressbar.animate(1, { duration: this.product.timeleft });
    }
    if (!this.product.managerUnlocked) {
      this.service.putProduct(this.product);
    }
  }

  calcScore() {
    this.quantityGain = this.calcGainUnlocks();
    if (this.product.timeleft > 0) {
      this.product.timeleft = this.product.timeleft - (Date.now() - this.lastUpdate);
      this.lastUpdate = Date.now();
      if (this.product.timeleft <= 0) {
        this.product.timeleft = 0;
        this.notifyProduction.emit(this.product);
        this.progressbar.set(0);
        this.isRunning = false;
        if (this.product.managerUnlocked) {
          this.startFabrication();
        }
      }
    }
  }

  calcMaxCanBuy() {
    if (this._qtmulti === 'Max') {
      // ln(((qX - X)/c) +1 )/ ln(q)
      // tslint:disable-next-line:max-line-length
      const quantity = Math.log(((this.product.croissance * this.money - this.money) / this.product.cout) + 1) / Math.log(this.product.croissance);
      this.nbProduit = +Math.trunc(quantity).toString();
    } else {
      this.nbProduit = +this._qtmulti;
    }
    this.calcPrice();
    this.checkBuyable();

  }

  calcPrice() {
    return this.totalPrice = this.product.cout * ((1 - Math.pow(this.product.croissance, this.nbProduit))) / (1 - this.product.croissance);
  }

  checkBuyable() {
    this.isBuyable = (this.totalPrice > this.money || this.nbProduit === 0) ? false : true;
  }

  OnBuy() {
    // Emit new world money
    this.money = this.money - this.calcPrice();
    this.notifyMoney.emit(this.money);
    // MAJ du cout
    this.product.cout = this.product.cout * Math.pow(this.product.croissance, this.nbProduit);
    // MAJ quantité
    this.product.quantite += this.nbProduit;
    this.quantityGain = this.calcGainUnlocks();
    this.notifyBuy.emit(this.product.quantite);
    // Check unlock
    this.checkUnlock();
    this.service.putProduct(this.product);
  }

  checkUnlock() {
    // Check unlock
    for (const unlock of this.product.palliers.pallier) {
      if (!unlock.unlocked && this.product.quantite > unlock.seuil) {
        // Si c'était locked et qu'on a dépassé le seuil, on achète l'unlock
        unlock.unlocked = true;
        this.openSnackBar();
        this.calcUpgrade(unlock);
      }
    }
  }

  openSnackBar() {
    this.snackBar.open('Bonus débloqué !', 'Fermer', { duration: 1000 });
  }

  calcGainUnlocks(): any {
    return this.product.quantite * this.product.revenu;
  }

  calcUpgrade(pallier: Pallier) {
    if (pallier.unlocked) {
      if (pallier.typeratio === 'vitesse') {
        if (this.product.timeleft > 0) {
          this.product.timeleft = this.product.timeleft / pallier.ratio;
          this.progressbar.animate(1, { duration: this.product.timeleft });
        }
        this.product.vitesse = this.product.vitesse / pallier.ratio;
      } else if (pallier.typeratio === 'gain') {
        this.product.revenu = this.product.revenu * pallier.ratio;
      }
    }
  }
}
