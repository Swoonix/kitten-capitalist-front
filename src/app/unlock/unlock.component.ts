import { Component, OnInit, Input } from '@angular/core';
import { Pallier } from '../world';
import { RestserviceService } from '../restservice.service';

@Component({
  selector: 'app-unlock',
  templateUrl: './unlock.component.html',
  styleUrls: ['./unlock.component.css']
})
export class UnlockComponent implements OnInit {

  unlockP: Pallier;
  imageUrl: string;

  @Input()
  set unlock(value: Pallier) {
    this.unlockP = value;
  }

  constructor(private service: RestserviceService) {
    this.imageUrl = this.service.getServerImage();
  }

  ngOnInit() {
  }

}
