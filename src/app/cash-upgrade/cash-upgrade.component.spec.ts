import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashUpgradeComponent } from './cash-upgrade.component';

describe('CashUpgradeComponent', () => {
  let component: CashUpgradeComponent;
  let fixture: ComponentFixture<CashUpgradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashUpgradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashUpgradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
