import { Pallier } from './../world';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cash-upgrade',
  templateUrl: './cash-upgrade.component.html',
  styleUrls: ['./cash-upgrade.component.css']
})
export class CashUpgradeComponent implements OnInit {

  upgradeP: Pallier;
  imageUrl: string;
  isBuyable: any;

  @Input() money: any;

  @Input()
  set upgrade(value: Pallier) {
    this.upgradeP = value;
  }

  @Output() notifyMoney: EventEmitter<any> = new EventEmitter();
  @Output() notifyUnlock: EventEmitter<any> = new EventEmitter();

  constructor(private service: RestserviceService, private snackBar: MatSnackBar) {
    this.imageUrl = this.service.getServerImage();
  }

  ngOnInit() {
    setInterval(() => { this.checkBuyable(); }, 100);
  }

  checkBuyable() {
    this.isBuyable = (this.money > this.upgradeP.seuil && !this.upgradeP.unlocked) ? true : false;
  }

  openSnackBar() {
    this.snackBar.open('Upgrade acheté !', 'Fermer', {duration: 1000});
  }

  buyUpgrade() {
    this.money = this.money - this.upgradeP.seuil;
    this.notifyMoney.emit(this.money);
    this.upgradeP.unlocked = true;
    this.notifyUnlock.emit(this.upgradeP.unlocked);
    this.openSnackBar();
    this.service.putUpgrade(this.upgradeP);
  }

}
