import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { World, Pallier, Product } from './world';

@Injectable({
  providedIn: 'root'
})
export class RestserviceService {
  // To be determined
  server = 'http://localhost:8080/adventureisis/';
  serverImg = 'http://localhost:8080';
  user = '';

  constructor(private http: HttpClient) {
    this.user = localStorage.getItem('username');
    if (this.user === '' || this.user === null) {
      const nb = Math.floor(Math.random() * 10000).toString();
      this.user = 'Kitten' + nb;
    }

    localStorage.setItem('username', this.user);

    this.getWorld();
  }

  /**
   * Retrieve the server url
   */
  getServer(): string {
    return this.server;
  }

  /**
   * Retrieve the icon folder in the server
   */
  getServerImage(): string {
    return this.serverImg;
  }

  /**
   * Retrieve the user
   */
  getUser() {
    return this.user;
  }


  /**
   * Set the user
   * @param user the user to be set
   */
  public setUser(user) {
    this.user = user;
  }

  /**
   * @param error the error
   */
  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  /**
   * set headers depending on the username
   * @param username the username
   */
  private setHeaders(username: string): HttpHeaders {
    return new HttpHeaders({ 'X-user': username });
  }

  /**
   * Retrieve the world from the back-end
   */
  getWorld(): Promise<World> {
    return this.http.get(this.server + 'generic/world', {
      headers: this.setHeaders(this.user)
    })
      .toPromise().catch(this.handleError);
  }

  public putProduct(product: Product): Promise<any> {
    return this.http.put(this.server + 'generic/product',
      product,
      {
        headers: this.setHeaders(this.user)
      })
      .toPromise();
  }

  public putManager(manager: Pallier): Promise<any> {
    return this.http.put(this.server + 'generic/manager',
      manager,
      {
        headers: this.setHeaders(this.user)
      })
      .toPromise();
  }

  public putUpgrade(upgrade: Pallier): Promise<any> {
    return this.http.put(this.server + 'generic/upgrade',
      upgrade,
      {
        headers: this.setHeaders(this.user)
      })
      .toPromise();
  }


}
