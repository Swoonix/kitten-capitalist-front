import { Component, OnInit, Input } from '@angular/core';
import { Pallier } from '../world';
import { RestserviceService } from '../restservice.service';

@Component({
  selector: 'app-product-unlock',
  templateUrl: './product-unlock.component.html',
  styleUrls: ['./product-unlock.component.css']
})
export class ProductUnlockComponent implements OnInit {

  unlockP: Pallier;
  imageUrl: string;

  @Input()
  set unlock(value: Pallier) {
    this.unlockP = value;
  }

  constructor(private service: RestserviceService) {
    this.imageUrl = this.service.getServerImage();
  }

  ngOnInit() {
  }

}
