import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUnlockComponent } from './product-unlock.component';

describe('ProductUnlockComponent', () => {
  let component: ProductUnlockComponent;
  let fixture: ComponentFixture<ProductUnlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUnlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUnlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
